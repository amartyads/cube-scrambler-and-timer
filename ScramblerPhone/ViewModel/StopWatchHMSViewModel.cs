﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Windows.UI.Xaml;
using ScramblerPhone.Model;

namespace ScramblerPhone.ViewModel
{
    class StopWatchHMSViewModel : INotifyPropertyChanged
    {
        private StopwatchModel _stopwatchModel = new StopwatchModel();
        private DispatcherTimer _timer = new DispatcherTimer();
        public TimeSpan TimeElapsed { get { return (TimeSpan)_stopwatchModel.Elapsed; } }
        public bool Running { get { return _stopwatchModel.Running; } }
        public StopWatchHMSViewModel()
        {
            _timer.Interval = TimeSpan.FromMilliseconds(50);
            _timer.Tick += TimerTick;
            _timer.Start();
            //Start();
        }
        public void Start()
        {
            _stopwatchModel.Start();
        }
        public void Stop()
        {
            _stopwatchModel.Stop();
        }
        public void Reset()
        {
            _stopwatchModel.Reset();
        }
        int _lastHours, _lastMinutes, _lastSeconds, _lastMilliseconds;
        void TimerTick(object sender, object e)
        {
            if (_lastHours != Hours)
            {
                _lastHours = Hours;
                OnPropertyChanged("Hours");
            }
            if(_lastMinutes != Minutes)
            {
                _lastMinutes = Minutes;
                OnPropertyChanged("Minutes");
            }
            if(_lastSeconds != Seconds)
            {
                _lastSeconds = Seconds;
                OnPropertyChanged("Seconds");
            }
            if(_lastMilliseconds != Milliseconds)
            {
                _lastMilliseconds = Milliseconds;
                OnPropertyChanged("Milliseconds");
            }
        }
        public int Hours { get { return _stopwatchModel.Elapsed.HasValue ? _stopwatchModel.Elapsed.Value.Hours : 0; } }
        public int Minutes { get { return _stopwatchModel.Elapsed.HasValue ? _stopwatchModel.Elapsed.Value.Minutes : 0; } }
        public int Seconds
        {
            get
            {
                if (_stopwatchModel.Elapsed.HasValue)
                {
                    return _stopwatchModel.Elapsed.Value.Seconds;
                }
                else
                    return 0;
            }
        }
        public int Milliseconds { get { return _stopwatchModel.Elapsed.HasValue ? _stopwatchModel.Elapsed.Value.Milliseconds : 0; } }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
