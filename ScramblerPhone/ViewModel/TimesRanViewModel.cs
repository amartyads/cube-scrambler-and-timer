﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ScramblerPhone.ViewModel
{
    [DataContract]
    class TimesRanViewModel
    {
        [DataMember]
        public int TimesLaunched { get; set; }
    }
}
