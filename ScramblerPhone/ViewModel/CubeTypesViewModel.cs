﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.ViewModel
{
    using Model;
    using System.Collections.ObjectModel;
    using ViewModel;
    class CubeTypesViewModel
    {
        public ObservableCollection<string> cubes = new ObservableCollection<string>{
            "2x2x2 cube",
            "3x3x3 cube",
            "4x4x4 cube",
            "5x5x5 cube",
            "6x6x6 cube",
            "7x7x7 cube",
            "8x8x8 cube",
            "9x9x9 cube",
            "10x10x10 cube",
            "11x11x11 cube",
            "Pyraminx",
            "Megaminx",
            "Square One",
            "UFO"
        };
    }
}
