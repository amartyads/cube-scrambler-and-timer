﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScramblerPhone.Model;

namespace ScramblerPhone.ViewModel
{
    class StatsCalculatorViewModel
    {
        public static StatsViewModel CalculateAll(StatsViewModel curStat, TimeSpan tm)
        {
            StatsModel rawStat = curStat.stats;
            StatsViewModel toRet;
            rawStat.NoSolved++;
            //best
            if (rawStat.Best.Ticks > tm.Ticks)
                rawStat.Best = tm;
            //average
            if (rawStat.NoSolved == 1)
                rawStat.Average = tm;
            else
                rawStat.Average = TimeSpan.FromTicks(((rawStat.Average.Ticks)*(rawStat.NoSolved-1) + tm.Ticks) / (rawStat.NoSolved));
            //averagesq
            if (rawStat.NoSolved == 1)
                rawStat.AverageSq = TimeSpan.FromTicks(tm.Ticks * tm.Ticks);
            else
                rawStat.AverageSq = TimeSpan.FromTicks(((rawStat.AverageSq.Ticks)*(rawStat.NoSolved-1) + (tm.Ticks * tm.Ticks)) / (rawStat.NoSolved));
            //last5
            for(int i = 0; i < 4; i++)
            {
                rawStat.Last5[i] = rawStat.Last5[i + 1];
            }
            rawStat.Last5[4] = tm;
            //last10
            for (int i = 0; i < 9; i++)
            {
                rawStat.Last10[i] = rawStat.Last10[i + 1];
            }
            rawStat.Last10[9] = tm;
            //best5
            bool maxExist = false;
            for (int i = 0; i < 5; i++)
            {
                if (rawStat.Best5[i] == TimeSpan.MaxValue)
                    maxExist = true;
            }
            for (int i = 0; i < 5; i++)
            {
                if(rawStat.Best5[i].Ticks > tm.Ticks)
                {
                    if (maxExist && rawStat.Best5[i] != TimeSpan.MaxValue)
                        continue;
                    else
                    {
                        rawStat.Best5[i] = tm;
                        break;
                    }
                }
            }
            //best10
            for (int i = 0; i < 10; i++)
            {
                if (rawStat.Best10[i] == TimeSpan.MaxValue)
                    maxExist = true;
            }
            for (int i = 0; i < 10; i++)
            {
                if (rawStat.Best10[i].Ticks > tm.Ticks)
                {
                    if (maxExist && rawStat.Best10[i] != TimeSpan.MaxValue)
                        continue;
                    else
                    {
                        rawStat.Best10[i] = tm;
                        break;
                    }
                }
            }
            toRet = new StatsViewModel(rawStat);
            toRet.StdDev = TimeSpan.FromMilliseconds(Convert.ToInt64(rawStat.AverageSq.TotalMilliseconds - (rawStat.Average.TotalMilliseconds * rawStat.Average.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            toRet.AvgLast5 = TimeSpan.FromMilliseconds(Convert.ToInt64(rawStat.Last5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            toRet.AvgLast10 = TimeSpan.FromMilliseconds(Convert.ToInt64(rawStat.Last10.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            toRet.AvgBest5 = TimeSpan.FromMilliseconds(Convert.ToInt64(rawStat.Best5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            toRet.AvgBest10 = TimeSpan.FromMilliseconds(Convert.ToInt64(rawStat.Best10.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            return toRet;
        }
    }
}
