﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    class ScrMega : IScrambler
    {
        private int _linelen;
        private int _linebr;
        private Random _randomizer;
        private List<int> _seq;
        private string _scramble;
        public ScrMega()
        {
            _linelen = 10;
            _linebr = 7;
            _seq = new List<int>();
            _randomizer = new Random();
            _scramble = "";
        }
        public ScrMega(int linelen, int linebr)
        {
            _linelen = linelen;
            _linebr = linebr;
            _seq = new List<int>();
            _randomizer = new Random();
            _scramble = "";
        }
        public string GetScramble()
        {
            return GetScramble(_linelen, _linebr);
        }
        public string GetScramble(int linelen, int linebr)
        {
            _linelen = linelen;
            _linebr = linebr;
            doScramble();
            _scramble = scrambleString();
            return _scramble;
        }
        private void doScramble()
        {
            int i, n;
            for (n = 0; n < 1; n++)
            {
                _seq = new List<int>();
                for (i = 0; i < _linebr * _linelen; i++)
                {
                    _seq.Add(_randomizer.Next(2));
                }
            }
        }
        private string scrambleString()
        {
            string s = "";
            int i, j;
            for (j = 0; j < _linebr; j++)
            {
                for (i = 0; i < _linelen; i++)
                {
                    if (i % 2 != 0)
                    {
                        if (_seq[j * _linelen + i] != 0) s += "D++ ";
                        else s += "D-- ";
                    }
                    else
                    {
                        if (_seq[j * _linelen + i] != 0) s += "R++ ";
                        else s += "R-- ";
                    }
                }
                if (_seq[(j + 1) * _linelen - 1] != 0) s += "U\n";
                else s += "U'\n";
            }
            return s;
        }
    }
}
