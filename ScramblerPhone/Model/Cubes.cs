﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    enum Cubes
    {
        Two = 0,
        Three = 1,
        Four = 2,
        Five = 3,
        Six = 4,
        Seven = 5,
        Eight = 6,
        Nine = 7,
        Ten = 8,
        Eleven = 9,
        Pyra = 10,
        Mega = 11,
        SqOne = 12,
        UFO = 13,
    }
}
