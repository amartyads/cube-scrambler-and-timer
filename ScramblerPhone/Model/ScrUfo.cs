﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    class ScrUfo : IScrambler
    {
        private int _scrmLen;
        private string _scramble;
        private List<int> _seq;
        private Random _randomizer;
        public ScrUfo()
        {
            _scrmLen = 20;
            _scramble = "";
            _seq = new List<int>();
            _randomizer = new Random();
        }
        public ScrUfo(int scrmLen)
        {
            _scrmLen = scrmLen;
            _scramble = "";
            _seq = new List<int>();
            _randomizer = new Random();
        }
        public string GetScramble()
        {
            return GetScramble(20);
        }
        public string GetScramble(int scrmLen)
        {
            _scrmLen = scrmLen;
            doScramble();
            _scramble = scrambleString();
            return _scramble;
        }
        private void doScramble()
        {
            //set up bookkeeping
            int lm = -1;
            // for each cube scramble
            for (int n = 0; n < 1; n++)
            {
                // initialise this scramble
                lm = -1;
                _seq = new List<int>(); // moves generated so far

                // while generated sequence not long enough
                while (_seq.Count < _scrmLen)
                {

                    // choose a different move than previous one
                    int m;
                    do
                    {
                        m = _randomizer.Next(4); // U, C,B,A
                    } while (m == lm);

                    // choose random amount
                    int q = (m == 0) ? _randomizer.Next(5) + 1 : 1;

                    // store move
                    _seq.Add(q * 4 + m);

                    lm = m;
                }
            }
        }
        private string scrambleString()
        {
            string s = "";
            for (int i = 0; i < _seq.Count; i++)
            {
                if (i != 0) s += " ";
                int q = _seq[i] >> 2;
                int m = _seq[i] & 3;
                s += "UCBA".ElementAt(m);
                if (m == 0) s += "012345".ElementAt(q);
            }
            return s;
        }
    }
}
