﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    interface IScrambler
    {
        string GetScramble();
    }
}
