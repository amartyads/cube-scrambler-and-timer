﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    class ScrCubes : IScrambler
    {
        public string _scramble;
        public int _cubeSize;
        public int _scrmLen;
        public List<int> _seq;
        public Random _randomizer;
        public bool _multiSlice;
        public ScrCubes()
        {
            _scramble = "";
            _cubeSize = 3;
            _scrmLen = 25;
            _seq = new List<int>();
            _randomizer = new Random();
            _multiSlice = false;
        }
        public ScrCubes(int cubeSize, int scrmLen)
        {
            _scramble = "";
            _cubeSize = cubeSize;
            _scrmLen = scrmLen;
            _seq = new List<int>();
            _randomizer = new Random();
            _multiSlice = false;
        }
        public string GetScramble(int cubeSize, int scrmLen)
        {
            _cubeSize = cubeSize;
            _scrmLen = scrmLen;
            doScramble();
            _scramble = scrambleString();
            return _scramble;
        }
        public string GetScramble()
        {
            return GetScramble(_cubeSize, _scrmLen);
        }
        private void doScramble()
        {
            //tl=number of allowed moves (twistable layers) on axis -- middle layer ignored
            int tl = _cubeSize;
            if (_multiSlice || _cubeSize % 2 != 0) tl--;
            //set up bookkeeping
            int[] axsl = new int[tl];    // movement of each slice/movetype on this axis
            int[] axam = {0,0,0}; // number of slices moved each amount
            int la; // last axis moved

            // for each cube scramble
            for (int n = 0; n < 1; n++)
            {
                // initialise this scramble
                la = -1;
                _seq = new List<int>(); // moves generated so far
                // reset slice/direction counters
                for (int i = 0; i < tl; i++) axsl[i] = 0;
                axam[0] = axam[1] = axam[2] = 0;
                int moved = 0;

                // while generated sequence not long enough
                while (_seq.Count + moved < _scrmLen)
                {
                    int ax, sl, q;
                    do
                    {
                        do
                        {
                            // choose a random axis
                            ax = _randomizer.Next(3);
                            // choose a random move type on that axis
                            sl = _randomizer.Next(tl);
                            // choose random amount
                            q = _randomizer.Next(3);
                        } while (ax == la && axsl[sl] != 0);		// loop until have found an unused movetype
                    } while (ax == la					// loop while move is reducible: reductions only if on same axis as previous moves
                            && !_multiSlice				// multislice moves have no reductions so always ok
                            && tl == _cubeSize			// only even-sized cubes have reductions (odds have middle layer as reference)
                            && (
                                2 * axam[0] == tl ||	// reduction if already have half the slices move in same direction
                                2 * axam[1] == tl ||
                                2 * axam[2] == tl ||
                                (
                                    2 * (axam[q] + 1) == tl	// reduction if move makes exactly half the slices moved in same direction and
                                    &&
                                    axam[0] + axam[1] + axam[2] - axam[q] > 0 // some other slice also moved
                                )
                            )
                    );

                    // if now on different axis, dump cached moves from old axis
                    if (ax != la)
                    {
                        appendMoves(_seq, axsl, tl, la);
                        // reset slice/direction counters
                        for (int i = 0; i < tl; i++) axsl[i] = 0;
                        axam[0] = axam[1] = axam[2] = 0;
                        moved = 0;
                        // remember new axis
                        la = ax;
                    }

                    // adjust counters for this move
                    axam[q]++;// adjust direction count
                    moved++;
                    axsl[sl] = q + 1;// mark the slice has moved amount

                }
                // dump the last few moves
                appendMoves( _seq, axsl, tl, la);

                // do a random cube orientation if necessary
                //seq[n][seq[n].length] = cubeorient ? Math.floor(Math.random() * 24) : 0;
            }
        }
        private void appendMoves(List<int> sq, int[] axsl, int tl, int la )
        {
            for (int sl = 0; sl < tl; sl++)// for each move type
            {
                if (axsl[sl] != 0) // if it occurs
                {				
			        int q=axsl[sl]-1;

			        // get semi-axis of this move
			        int sa = la;
			        int m = sl;
			        if(sl+sl+1>=tl){ // if on rear half of this axis
				        sa+=3; // get semi-axis (i.e. face of the move)
				        m=tl-1-m; // slice number counting from that face
				        q=2-q; // opposite direction when looking at that face
			        }
			        // store move
			        sq.Add((m*6+sa)*4+q);
		        }
	        }
        }
        private string scrambleString()
        {
            string s = "";
            int j;
            for (int i = 0; i < _seq.Count ; i++)
            {
                if (i != 0) s += " ";
                int k = _seq[i] >> 2;
                j = k % 6;
                double l = (k - j) / 6;
                
                if (l != 0 && _cubeSize <= 5 && !_multiSlice)
                {
                    s += "dlburf".ElementAt(j);	// use lower case only for inner slices on 4x4x4 or 5x5x5
                }
                else
                {
                    if (_cubeSize <= 5 && _multiSlice)
                    {
                        s += "DLBURF".ElementAt(j);
                        if (l != 0) s += "w";	// use w only for double layers on 4x4x4 and 5x5x5
                    }
                    else
                    {
                        if (l != 0) s += ((int)l + 1);
                        s += "DLBURF".ElementAt(j);
                    }
                }

                j = _seq[i] & 3;
                if (j != 0) s += " 2'".ElementAt(j);
            }

            // add cube orientation
            /*if (cubeorient)
            {
                var ori = seq[n][seq[n].length - 1];
                s = "Top:" + colorList[2 + colors[colorPerm[ori][3]]]
                    + "&nbsp;&nbsp;&nbsp;Front:" + colorList[2 + colors[colorPerm[ori][5]]] + "<br>" + s;
            }
             */
            return s;
        }
    }
}
