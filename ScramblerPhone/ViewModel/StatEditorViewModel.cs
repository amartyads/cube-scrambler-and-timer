﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Runtime.Serialization;
using ScramblerPhone.Model;

namespace ScramblerPhone.ViewModel
{
    class StatEditorViewModel
    {
        public async static Task CheckInitials()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(StatsModel));
            DataContractSerializer checker = new DataContractSerializer(typeof(TimesRanViewModel));
            IStorageFolder localFolder = ApplicationData.Current.LocalFolder;
            IStorageFile checkRan;
            bool filesExist = true;
            try
            {
                checkRan = await localFolder.GetFileAsync("TimesRan.xml");
            }
            catch(FileNotFoundException)
            {
                filesExist = false;
            }
            if(!filesExist)
            {
                checkRan = await localFolder.CreateFileAsync("TimesRan.xml");
                using(IRandomAccessStream stream = await checkRan.OpenAsync(FileAccessMode.ReadWrite))
                using(Stream outputStream = stream.AsStreamForWrite())
                {
                    checker.WriteObject(outputStream, new TimesRanViewModel() { TimesLaunched = 1 });
                }
                CubeTypesViewModel cb = new CubeTypesViewModel();
                List<string> cubeList = new List<string>();
                foreach(string temp in cb.cubes)
                {
                    cubeList.Add(temp);
                }
                foreach(string temp in cubeList)
                {
                    StatsModel svm = new StatsModel();
                    svm.CubeName = temp;
                    IStorageFile statFile = await localFolder.CreateFileAsync(temp+".xml",CreationCollisionOption.ReplaceExisting);
                    using(IRandomAccessStream stream = await statFile.OpenAsync(FileAccessMode.ReadWrite))
                    using(Stream outputStream = stream.AsStreamForWrite())
                    {
                        serializer.WriteObject(outputStream,svm);
                    }
                }
            }
            else
            {
                checkRan = await localFolder.GetFileAsync("TimesRan.xml");
                TimesRanViewModel tm;
                using(IRandomAccessStream stream = await checkRan.OpenAsync(FileAccessMode.Read))
                using(Stream inputStream = stream.AsStreamForRead())
                {
                    tm = checker.ReadObject(inputStream) as TimesRanViewModel;
                }
                tm.TimesLaunched++;
                using(IRandomAccessStream stream = await checkRan.OpenAsync(FileAccessMode.ReadWrite))
                using(Stream outputStream = stream.AsStreamForWrite())
                {
                    checker.WriteObject(outputStream, tm);
                }
            }
        }
    }
}
