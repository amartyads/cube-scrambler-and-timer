﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.ViewModel
{
    using Model;
    using ViewModel;
    class GetScrambleViewModel
    {
        private string scramble;
        public string Scramble { get { return scramble; } }
        public GetScrambleViewModel(int n)
        {
            scramble = makeScramble(n);
        }
        public GetScrambleViewModel()
        {
            scramble = makeScramble(3);
        }
        private string makeScramble(int n)
        {
            IScrambler ob;
            Cubes choice = (Cubes)n;
            ob = new ScrCubes();
            switch (choice)
            {
                case Cubes.Two:
                    ob = new ScrCubes(2, 9);
                    break;
                case Cubes.Three:
                    ob = new ScrCubes(3, 25);
                    break;
                case Cubes.Four:
                    ob = new ScrCubes(4, 40);
                    break;
                case Cubes.Five:
                    ob = new ScrCubes(5, 60);
                    break;
                case Cubes.Six:
                    ob = new ScrCubes(6, 80);
                    break;
                case Cubes.Seven:
                    ob = new ScrCubes(7, 90);
                    break;
                case Cubes.Eight:
                    ob = new ScrCubes(8, 100);
                    break;
                case Cubes.Nine:
                    ob = new ScrCubes(9, 110);
                    break;
                case Cubes.Ten:
                    ob = new ScrCubes(10, 120);
                    break;
                case Cubes.Eleven:
                    ob = new ScrCubes(11, 130);
                    break;
                case Cubes.Pyra:
                    ob = new ScrPyra();
                    break;
                case Cubes.Mega:
                    ob = new ScrMega();
                    break;
                case Cubes.SqOne:
                    ob = new ScrSqOne();
                    break;
                case Cubes.UFO:
                    ob = new ScrUfo();
                    break;
                default:
                    break;
            }
            return ob.GetScramble();
        }
    }
}
