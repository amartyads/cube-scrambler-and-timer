﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScramblerPhone.Model
{
    class ScrSqOne : IScrambler
    {
        private string _scramble;
        private int _scrmLen;
        private List<int> _seq;
        private Random _randomizer;
        private int[] posit = { 0, 0, 1, 2, 2, 3, 4, 4, 5, 6, 6, 7, 8, 9, 9, 10, 11, 11, 12, 13, 13, 14, 15, 15 };
        public ScrSqOne()
        {
            _scrmLen = 20;
            _scramble = "";
            _seq = new List<int>();
            _randomizer = new Random();
        }
        public ScrSqOne(int scrmLen)
        {
            _scrmLen = scrmLen;
            _scramble = "";
            _seq = new List<int>();
            _randomizer = new Random();
        }
        public string GetScramble()
        {
            return GetScramble(_scrmLen);
        }
        public string GetScramble(int scrmLen)
        {
            _scrmLen = scrmLen;
            doScramble();
            _scramble = scrambleString();
            return _scramble;
        }
        private void doScramble()
        {
            int i, j, ls, n, f;
            for (n = 0; n < 1; n++)
            {
                ls = -1;
                _seq = new List<int>();
                f = 0;
                for (i = 0; i < _scrmLen; i++)
                {
                    do
                    {
                        if (ls == 0)
                        {
                            j = _randomizer.Next(22) - 11;
                            if (j >= 0) j++;
                        }
                        else if (ls == 1)
                        {
                            j = _randomizer.Next(12) - 11;
                        }
                        else if (ls == 2)
                        {
                            j = 0;
                        }
                        else
                        {
                            j = _randomizer.Next(23) - 11;
                        }
                        // if past second twist, restrict bottom layer
                    } while ((f > 1 && j >= -6 && j < 0) || doMove(j));
                    if (j > 0) ls = 1;
                    else if (j < 0) ls = 2;
                    else { ls = 0; f++; }
                    _seq.Add(j);
                }
            }
        }

        private bool doMove(int j)
        {
            int i, c, f = j;
            List<int> t;
            //do move f
            if (f == 0)
            {
                for (i = 0; i < 6; i++)
                {
                    c = posit[i + 12];
                    posit[i + 12] = posit[i + 6];
                    posit[i + 6] = c;
                }
            }
            else if (f > 0)
            {
                f = 12 - f;
                if (posit[f] == posit[f - 1]) return true;
                if (f < 6 && posit[f + 6] == posit[f + 5]) return true;
                if (f > 6 && posit[f - 6] == posit[f - 7]) return true;
                if (f == 6 && posit[0] == posit[11]) return true;
                t = new List<int>();
                for (i = 0; i < 12; i++) t.Add(posit[i]);
                c = f;
                for (i = 0; i < 12; i++)
                {
                    posit[i] = t[c];
                    if (c == 11) c = 0; else c++;
                }
            }
            else if (f < 0)
            {
                f = -f;
                if (posit[f + 12] == posit[f + 11]) return true;
                if (f < 6 && posit[f + 18] == posit[f + 17]) return true;
                if (f > 6 && posit[f + 6] == posit[f + 5]) return true;
                if (f == 6 && posit[12] == posit[23]) return true;
                t = new List<int>();
                for (i = 0; i < 12; i++) t.Add(posit[i + 12]);
                c = f;
                for (i = 0; i < 12; i++)
                {
                    posit[i + 12] = t[c];
                    if (c == 11) c = 0; else c++;
                }
            }
            return false;
        }
        private string scrambleString()
        {
            string s = "";
            int i, k, l = -1;
            for (i = 0; i < _seq.Count; i++)
            {
                k = _seq[i];
                if (k == 0)
                {
                    if (l == -1) s += "(0,0)  ";
                    if (l == 1) s += "0)  ";
                    if (l == 2) s += ")  ";
                    l = 0;
                }
                else if (k > 0)
                {
                    s += "(" + (k > 6 ? k - 12 : k) + ",";
                    l = 1;
                }
                else if (k < 0)
                {
                    if (l <= 0) s += "(0,";
                    s += (k <= -6 ? k + 12 : k);
                    l = 2;
                }
            }
            if (l == 1) s += "0";
            if (l != 0) s += ")";
            //if(l==0) s+="(0,0)";
            return s;
        }
    }
}
