﻿using ScramblerPhone.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ScramblerPhone.View;
using ScramblerPhone.ViewModel;
using Windows.System.Display;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace ScramblerPhone
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private bool _appJustLaunched = true;
        private StatsManagerViewModel _manager;
        DisplayRequest displayRequest;
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            displayRequest = new DisplayRequest();
            displayRequest.RequestActive();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            CubeTypesViewModel cb = new CubeTypesViewModel();
            ComboBoxPuzName.DataContext = cb.cubes;
            _manager = new StatsManagerViewModel();
            //ComboBoxPuzName.SelectedIndex = 1;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (_appJustLaunched)
            {
                await StatEditorViewModel.CheckInitials();
                SetScramble(1);

                await _manager.MakeStats();
                ComboBoxPuzName.SelectedIndex = 1;
                _appJustLaunched = false;
                statsViewModel = _manager.GetStat(1);
                statsViewModel.CheckChanged();
                UpdateStatBoxes();
            }
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void ComboBoxPuzName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetScramble(ComboBoxPuzName.SelectedIndex);
            SetTextBoxStyle(ComboBoxPuzName.SelectedIndex);
            statsViewModel = _manager.GetStat(ComboBoxPuzName.SelectedIndex);
            statsViewModel.CheckChanged();
            UpdateStatBoxes();
        }
        private void SetScramble(int n)
        {
            GetScrambleViewModel gs = new GetScrambleViewModel(n);
            TextBlockScramble.Text = gs.Scramble;
        }

        private void AppBarButtonNewScramble_Click(object sender, RoutedEventArgs e)
        {
            SetScramble(ComboBoxPuzName.SelectedIndex);
        }
        private void SetTextBoxStyle(int n)
        {
            if(n <= 2 || (n >= 10 && n != 11))
            {
                TextBlockScramble.FontSize = 30;
                TextBlockScramble.LineHeight = 35;
            }
            else if(n <= 3)
            {
                TextBlockScramble.FontSize = 23;
                TextBlockScramble.LineHeight = 28;
            }
            else if(n <= 5)
            {
                TextBlockScramble.FontSize = 17;
                TextBlockScramble.LineHeight = 20;
            }
            else if(n <= 8 || n == 11)
            {
                TextBlockScramble.FontSize = 15;
                TextBlockScramble.LineHeight = 17;
            }
            else
            {
                TextBlockScramble.FontSize = 14;
                TextBlockScramble.LineHeight = 15;
            }
        }

        private void TextBlockScramble_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(SeeFullScrambleView), TextBlockScramble.Text);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (stopWatchHmsViewModel.Running)
            {
                stopWatchHmsViewModel.Stop();
                ButtonStartStop.Content = "Start";
                ContentDialog dialog = new ContentDialog()
                {
                    Content = "Would you like to save this time?",
                    PrimaryButtonText = "Yes",
                    SecondaryButtonText = "No"
                };
                ContentDialogResult result = await dialog.ShowAsync();
                if (result == ContentDialogResult.Primary)
                {
                    statsViewModel = _manager.UpdateStat(ComboBoxPuzName.SelectedIndex, statsViewModel, stopWatchHmsViewModel.TimeElapsed);
                    statsViewModel.CheckChanged();
                    UpdateStatBoxes();
                    SetScramble(ComboBoxPuzName.SelectedIndex);
                }
                SetTapEnables(true);
            }
            else
            {
                stopWatchHmsViewModel.Reset();
                stopWatchHmsViewModel.Start();
                ButtonStartStop.Content = "Stop";
                SetTapEnables(false);
            }
        }
        private void SetTapEnables(bool b)
        {
            ComboBoxPuzName.IsTapEnabled = b;
            TextBlockScramble.IsTapEnabled = b;
            StackPanelStats.IsTapEnabled = b;
        }
        private void UpdateStatBoxes()
        {
            TextBoxAvg.Text = ("Average of " + statsViewModel.NumberSolved + ": "+ statsViewModel.Average);
            TextBoxBest.Text = "Best: " + statsViewModel.Best;
        }

        private void StackPanelStats_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(SeeFullStatsView), statsViewModel);
        }

        private void AppBarButtonClearStats_Click(object sender, RoutedEventArgs e)
        {
            statsViewModel = _manager.ClearStat(ComboBoxPuzName.SelectedIndex, statsViewModel);
            statsViewModel.CheckChanged();
            UpdateStatBoxes();
        }
    }
}
