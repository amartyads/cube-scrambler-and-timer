﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScramblerPhone.Model;
using System.ComponentModel;

namespace ScramblerPhone.ViewModel
{
    class StatsViewModel : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string Best
        {
            get 
            {
                try
                {
                    if (NumberSolved > 0)
                        return stats.Best.ToString(@"hh\:mm\:ss\:fff");
                    else
                        return "00:00:00:000";
                }
                catch(NullReferenceException)
                {
                    return "00:00:00:000";
                }
            } 
            set 
            { ; } 
        }
        public string Average
        {
            get
            {
                try
                {
                    if (NumberSolved > 0)
                        return stats.Average.ToString(@"hh\:mm\:ss\:fff");
                    else
                        return "00:00:00:000";
                }
                catch (NullReferenceException)
                {
                    return "00:00:00:000";
                }
            }
            set
            { ; }
        }
        public int NumberSolved { get; set; }
        public string StdDev { get; set; }
        public string AvgLast5
        {
            get;
            set;
        }
        public string AvgBest5 { get; set; }
        public string AvgLast10 { get; set; }
        public string AvgBest10 { get; set; }
        public StatsModel stats;
        public StatsViewModel(StatsModel stats)
        {
            this.stats = stats;
            Name = stats.CubeName;
            Best = stats.Best.ToString();
            Average = stats.Average.ToString();
            NumberSolved = stats.NoSolved;
        }
        public StatsViewModel()
        {
            this.stats = null;
        }
        public void CheckChanged()
        {
            OnPropertyChanged("Best");
            OnPropertyChanged("Average");
            OnPropertyChanged("NumberSolved");
            if(NumberSolved < 10)
            {
                if(NumberSolved < 5)
                {
                    AvgLast5 = AvgBest5 = "N/A";
                }
                else
                {
                    AvgLast5 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Last5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
                    AvgBest5 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Best5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
                }
                AvgLast10 = AvgBest10 = "N/A";
            }
            else
            {
                AvgLast5 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Last5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
                AvgBest5 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Best5.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
                AvgLast10 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Last10.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
                AvgBest10 = TimeSpan.FromMilliseconds(Convert.ToInt64(stats.Best10.Average(ts => ts.TotalMilliseconds))).ToString(@"hh\:mm\:ss\:fff");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if (propertyChanged != null)
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
