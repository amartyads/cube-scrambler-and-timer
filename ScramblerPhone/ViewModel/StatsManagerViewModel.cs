﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScramblerPhone.Model;

using Windows.Storage;
using Windows.Storage.Streams;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.IO;

namespace ScramblerPhone.ViewModel
{
    class StatsManagerViewModel
    {
        private StatsViewModel[] stats;

        DataContractSerializer serializer = new DataContractSerializer(typeof(StatsModel));
        IStorageFolder localFolder = ApplicationData.Current.LocalFolder;
        public StatsManagerViewModel()
        {
            stats = new StatsViewModel[14];
            //MakeStats();
        }
        public async Task MakeStats()
        {
            CubeTypesViewModel cb = new CubeTypesViewModel();
            for(int i = 0; i <= 13; i++)
            {
                string curCube = cb.cubes[i];
                StatsModel temp = null;
                IStorageFile statFile = await localFolder.GetFileAsync(curCube + ".xml");
                using(IRandomAccessStream stream = await statFile.OpenAsync(FileAccessMode.Read))
                using(Stream inputStream = stream.AsStreamForRead())
                {
                    temp = serializer.ReadObject(inputStream) as StatsModel;
                }
                stats[i] = new StatsViewModel(temp);

            }
        }
        public StatsViewModel ClearStat(int n, StatsViewModel sn)
        {
            CubeTypesViewModel cb = new CubeTypesViewModel();
            StatsModel st = new StatsModel();
            st.CubeName = cb.cubes[n];
            stats[n] = new StatsViewModel(st);
            WriteStats(n);
            return stats[n];
        }
        public StatsViewModel GetStat(int n)
        {
            return stats[n];
        }
        public StatsViewModel UpdateStat(int n, StatsViewModel sn, TimeSpan tm)
        {
            stats[n] = StatsCalculatorViewModel.CalculateAll(sn, tm);
            WriteStats(n);
            return stats[n];
        }
        public async void WriteStats(int n)
        {
            IStorageFile statFile = await localFolder.CreateFileAsync(stats[n].Name + ".xml", CreationCollisionOption.ReplaceExisting);
            using(IRandomAccessStream stream = await statFile.OpenAsync(FileAccessMode.ReadWrite))
            using(Stream outputStream = stream.AsStreamForWrite())
            {
                serializer.WriteObject(outputStream, stats[n].stats);
            }
        }
    }
}
