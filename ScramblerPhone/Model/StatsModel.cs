﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ScramblerPhone.Model
{
    [DataContract]
    class StatsModel
    {
        [DataMember]
        public string CubeName { get; set; }
        [DataMember]
        public TimeSpan Average { get; set; }
        [DataMember]
        public TimeSpan AverageSq { get; set; }
        [DataMember]
        public TimeSpan[] Last5 { get; set; }
        [DataMember]
        public TimeSpan[] Best5 { get; set; }
        [DataMember]
        public TimeSpan[] Last10 { get; set; }
        [DataMember]
        public TimeSpan[] Best10 { get; set; }
        [DataMember]
        public int NoSolved { get; set; }
        [DataMember]
        public TimeSpan Best { get; set; }
        public StatsModel()
        {
            CubeName = "";
            Average = TimeSpan.MaxValue;
            AverageSq = TimeSpan.MaxValue;
            Best = TimeSpan.MaxValue;
            Last5 = Best5 = new TimeSpan[5];
            Last10 = Best10 = new TimeSpan[10];
            for(int i = 0; i < 10; i++)
            {
                if(i<5)
                    Last5[i] = Best5[i] = TimeSpan.MaxValue;
                Last10[i] = Best10[i] = TimeSpan.MaxValue;
            }
        }
    }
}
